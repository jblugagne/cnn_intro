from pathlib import Path
import time

import numpy as np
import cv2
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader, TensorDataset

#%% Load Chihuahua & Muffin images
path = Path("muffihuahua/train/")

images, labels_array = [], []
for label, category in enumerate(("muffin", "chihuahua")):
    cat_path = path / category
    for f, file in enumerate(cat_path.glob("*.jpg")):
        if f % 100 == 0:
            print(f"{f} Images loaded")
        image = cv2.imread(str(file))
        images.append(cv2.resize(image, (256, 256)))
        labels_array.append(label)
images = np.array(images)
labels_array = np.array(labels_array)

#%% Define and train VGG16
class VGG16(nn.Module):
    def __init__(self):
        super(VGG16, self).__init__()
        self.features = nn.Sequential(
            # Conv Layer block 1
            nn.Conv2d(in_channels=3, out_channels=16, kernel_size=3, padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.Conv2d(in_channels=16, out_channels=16, kernel_size=3, padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),

            # Conv Layer block 2
            nn.Conv2d(in_channels=16, out_channels=32, kernel_size=3, padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.Conv2d(in_channels=32, out_channels=32, kernel_size=3, padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),

            # Conv Layer block 3
            nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),

            # Conv Layer block 4
            nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),

            # Conv Layer block 5
            nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),
        )

        self.classifier = nn.Sequential(
            nn.Linear(128 * 8 * 8, 512),
            nn.ReLU(),
            nn.Dropout(0.5),
            nn.Linear(512, 512),
            nn.ReLU(),
            nn.Dropout(0.5),
            nn.Linear(512, 1)  # Output layer for binary classification
        )

    def forward(self, x):
        x = self.features(x)
        x = x.reshape(x.size(0), -1)  # Flatten the features
        x = self.classifier(x)
        return x


# Check for GPU availability
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"Using device: {device}")

# Hyperparameters
num_epochs = 200
batch_size = 32
learning_rate = 0.001

# Convert numpy images to tensors
images_tensor = torch.tensor(images.transpose((0, 3, 1, 2)), dtype=torch.float32).to(device)  # Adjust shape for PyTorch (N, C, H, W)
labels_tensor = torch.tensor(labels_array, dtype=torch.float32).unsqueeze(1).to(device)  # Add dimension for binary label

# Create Tensor datasets and DataLoader
dataset = TensorDataset(images_tensor, labels_tensor)
data_loader = DataLoader(dataset=dataset, batch_size=batch_size, shuffle=True)

# Initialize model
model = VGG16().to(device)

# Loss and optimizer
criterion = nn.BCEWithLogitsLoss()
optimizer = optim.Adam(model.parameters(), lr=learning_rate)

#%% Training loop

model.train()
for epoch in range(num_epochs):
    epoch_loss = 0.0
    for i, (inputs, labels) in enumerate(data_loader):
        # inputs, labels = inputs.to(device), labels.to(device)

        optimizer.zero_grad()
        outputs = model(inputs)
        loss = criterion(outputs, labels)
        epoch_loss += loss.item()
        loss.backward()
        optimizer.step()

    epoch_loss /= i+1
    print(f'Epoch [{epoch+1}/{num_epochs}], Loss: {loss.item():.4f}, Epoch Loss: {epoch_loss:.4f}')

# Save the model
torch.save(model.state_dict(), 'vgg16_binary_classification.pth')

#%% Evaluate on test images:

path = Path("muffihuahua/test/")

images = []
for file in path.glob("*"):
    image = cv2.imread(str(file))
    images.append(cv2.resize(image, (256, 256)))
images = np.array(images)
images_tensor = torch.tensor(images.transpose((0, 3, 1, 2)), dtype=torch.float32)  # Adjust shape for PyTorch (N, C, H, W)

model = VGG16().to(device)
model.load_state_dict(torch.load("vgg16_binary_classification.pth"))
model.eval()
with torch.no_grad():
    start = time.time()
    outputs = model(images_tensor.to(device))
    duration = time.time() - start
    outputs = outputs.cpu().detach().numpy()

print(f"17 images processed, {duration*1000/17:.1f} milliseconds per image")
indexes = np.random.choice(len(outputs), size=len(outputs), replace=False)

#%% Can you do better than a VGG16?

plt.figure(1, figsize=(10, 10))
plt.clf()
plt.pause(.5)
plt.figure(2, figsize=(10, 10))
plt.clf()
plt.pause(.5)

categories = ["muffin", "chihuahua"]

for index in indexes:
    input("Next?")
    plt.figure(2)
    plt.clf()
    plt.show()
    plt.figure(1)
    plt.clf()
    plt.pause(.5)
    plt.imshow(images[index,:,:,::-1])
    plt.axis('off')
    plt.tight_layout()
    plt.pause(.01)
    plt.clf()
    plt.pause(.1)
    input("Reveal answer?")
    plt.figure(2)
    plt.imshow(images[index,:,:,::-1])
    plt.title(
        f"CNN prediction: {categories[int(outputs[index,0]>0)]}",
        fontsize=20,
    )
    plt.axis('off')
    plt.tight_layout()
    plt.pause(.2)


#%% Show all results:

plt.figure(1)
plt.clf()
categories = ["muffin", "chihuahua"]
for index, (image, output) in enumerate(zip(images, outputs)):
    plt.subplot(4, 4, index + 1)
    plt.imshow(image[:,:,::-1])
    plt.title(categories[int(output>0)], fontsize=20)
    plt.axis('off')
    plt.tight_layout()
