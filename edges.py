import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import cv2

import torch
import torch.nn as nn
from torch.utils.data import DataLoader, TensorDataset

#%% Generate images with random rectangles and circles

# Initialize lists
nb_samples = 5_000
image_list = []
edges_list = []
laplacian_list = []

# Laplacian kernel:
kernel = np.array([[0, 1, 0], [1, -4, 1], [0, 1, 0]])

# Generate samples:
for i in range(nb_samples):

    if i % 100 == 0:
        print(f"{i} images generated")

    # Create new blank images:
    image = np.zeros((256, 256), dtype=np.float32)
    edges = image.copy()

    for element in range(np.random.randint(10)): # Add random number of shapes (between 0 and 9)
        color = np.random.uniform()
        shape = np.zeros_like(image)
        if np.random.randint(2): # Flip a coin to draw circle or recatngle
            cv2.circle(
                shape,
                center=tuple(np.random.randint(256, size=2)),
                radius=np.random.randint(64),
                color=1,
                thickness=-1,
            )
        else:
            cv2.rectangle(
                shape,
                pt1=tuple(np.random.randint(256, size=2)),
                pt2=tuple(np.random.randint(256, size=2)),
                color=1,
                thickness=-1,
            )
            # Apply random rotation to rectangle:
            angle = np.random.uniform()*45
            image_center = tuple(np.array(shape.shape[1::-1]) / 2)
            rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
            shape = cv2.warpAffine(shape, rot_mat, image.shape[1::-1], flags=cv2.INTER_NEAREST)

        # Add rectangle or circle to image:
        image[shape>0] = color

        # Add shape edges to edge
        edges[cv2.dilate(shape, np.ones((3,3)))>0] = 1
        edges[cv2.erode(shape, np.ones((5,5)))>0] = 0

    # Compute Laplacian of image
    laplacian = cv2.filter2D(src=image, ddepth=-1, kernel=kernel)

    # Add to lists
    image_list.append(image)
    edges_list.append(edges)
    laplacian_list.append(laplacian)

#%% Show data examples:

nb_examples = 3
examples = np.random.choice(nb_samples, size=nb_examples, replace=False)
plt.figure(1, figsize=(15, 15))
for index, example in enumerate(examples):
    plt.subplot(nb_examples, 3, 3*index+1)
    plt.imshow(image_list[example], cmap='gray', vmin=0, vmax=1)
    plt.axis("off")
    plt.subplot(nb_examples, 3, 3*index+2)
    plt.imshow(laplacian_list[example], cmap='gray', vmin=-1, vmax=1)
    plt.axis("off")
    plt.subplot(nb_examples, 3, 3*index+3)
    plt.imshow(edges_list[example], cmap='gray', vmin=0, vmax=1)
    plt.axis("off")
plt.subplot(nb_examples, 3, 1)
plt.title("Input image", fontsize=25)
plt.subplot(nb_examples, 3, 2)
plt.title("Laplacian", fontsize=25)
plt.subplot(nb_examples, 3, 3)
plt.title('"True" edges', fontsize=25)
plt.tight_layout()
plt.savefig("data_examples.png", dpi=300)
plt.show()

#%% Define Convolutional Neural Network

# Hyperparameters
batch_size = 32
learning_rate = 0.001
num_epochs = 50

# Model definition
class ConvNet(nn.Module):
    def __init__(self, filters=1, activation=None):
        super(ConvNet, self).__init__()
        self.conv1 = nn.Conv2d(
            in_channels=1, out_channels=filters, kernel_size=3, padding=1, bias=False
        )
        self.activation = activation

    def forward(self, x):
        x = self.conv1(x)
        if self.activation is not None:
            x = self.activation(x)
        x = torch.sum(x, dim=1, keepdim=True)
        return x


# Create input image tensor from numpy array:
input_images = torch.tensor(np.array(image_list)[:,None], dtype=torch.float32).to(device)

# Learn Laplacian filter:
groundtruth = torch.tensor(np.array(laplacian_list)[:, None], dtype=torch.float32).to(device)
model = ConvNet(filters=1).to(device)
criterion = nn.MSELoss()

# Learn 4 filters to identify edges:
# groundtruth = torch.tensor(np.array(edges_list)[:, None], dtype=torch.float32).to(device)
# model = ConvNet(filters=4, activation=torch.relu).to(device)
# criterion = nn.BCEWithLogitsLoss(
#     pos_weight=torch.FloatTensor([10.0]).to(device)
# )  # Class-weighted Binary Cross-Entropy

# Compile data loader and optimizer:
dataset = TensorDataset(input_images, groundtruth)
data_loader = DataLoader(dataset=dataset, batch_size=batch_size, shuffle=True)
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

# Show kernel(s) / weights:
weights = model.conv1.weight.detach().cpu().numpy()
plt.figure(2, figsize=(8*len(weights), 6))
plt.clf()
for k, kernel in enumerate(weights):
    plt.subplot(1, len(weights), k+1)
    plt.imshow(kernel[0], cmap="RdBu", vmin=-1, vmax=1)
    for x in range(3):
        for y in range(3):
            plt.text(x, y, f'{kernel[0, y, x]:.02f}', color="k", verticalalignment='center', horizontalalignment='center', fontsize=30)
    plt.title(f"Kernel {k+1}", fontsize=30)
    plt.axis("off")
plt.tight_layout()
plt.pause(.01)

#%% Training loop

for epoch in range(num_epochs):
    epoch_loss = 0.0
    for i, (inputs, labels) in enumerate(data_loader):
        # Forward pass
        outputs = model(inputs)
        loss = criterion(outputs, labels)
        epoch_loss += loss.item()

        # Backward and optimize
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    epoch_loss /= i+1
    print(f'Epoch [{epoch+1}/{num_epochs}], Loss: {loss.item():.4f}, Epoch Loss: {epoch_loss:.4f}')

    # Show kernel(s):
    weights = model.conv1.weight.detach().cpu().numpy()
    plt.clf()
    for k, kernel in enumerate(weights):
        plt.subplot(1, len(weights), k+1)
        plt.imshow(kernel[0], cmap="RdBu", vmin=-1, vmax=1)
        for x in range(3):
            for y in range(3):
                plt.text(x, y, f'{kernel[0, y, x]:.02f}', color="k", verticalalignment='center', horizontalalignment='center', fontsize=30)
        plt.title(f"Kernel {k+1}", fontsize=30)
        plt.axis("off")
        plt.tight_layout()
    plt.pause(.01)

print('Training completed.')

#%% Show Laplacian learning examples:

plt.figure(3, figsize=(20, 10))

nb_examples = 3
examples = np.random.choice(nb_samples, size=nb_examples, replace=False)
inputs = torch.tensor(np.array(image_list)[examples,None], dtype=torch.float32).to(device)

# Run model on examples:
model.eval()
with torch.no_grad():
    outputs = model(inputs).detach().cpu().numpy()

# Show input, output, and ground-truth examples:
for index, example in enumerate(examples):
    plt.subplot(nb_examples, 3, 3*index+1)
    plt.imshow(image_list[example], cmap='gray', vmin=0, vmax=1)
    plt.axis("off")
    plt.subplot(nb_examples, 3, 3*index+2)
    plt.imshow(outputs[index, 0], cmap='gray', vmin=-1, vmax=1)
    plt.axis("off")
    plt.subplot(nb_examples, 3, 3*index+3)
    plt.imshow(laplacian_list[example], cmap='gray', vmin=-1, vmax=1)
    plt.axis("off")
plt.subplot(nb_examples, 3, 1)
plt.title("Original Image", fontsize=20)
plt.subplot(nb_examples, 3, 2)
plt.title("CNN Output", fontsize=20)
plt.subplot(nb_examples, 3, 3)
plt.title("Ground truth", fontsize=20)
plt.tight_layout()
plt.show()

#%% Show Edge detection kernels and intermediate layers:

plt.figure(3, figsize=(20, 10))

# Get random examples:
nb_examples = 3
examples = np.random.choice(nb_samples, size=nb_examples, replace=False)
inputs = torch.tensor(np.array(image_list)[examples,None], dtype=torch.float32).to(device)

# Run model on examples:
model.eval()
with torch.no_grad():
    outputs = model(inputs).detach().cpu().numpy()
    intermediate = model.conv1(inputs).detach().cpu().numpy()

# Show kernels:
weights = model.conv1.weight.detach().cpu().numpy()
for index, kernel in enumerate(weights):
    plt.subplot(nb_examples+1, 6, index+2)
    plt.imshow(kernel[0], cmap="RdBu", vmin=-1, vmax=1)
    plt.axis("off")

# Show inputs, outputs, and intermediate results:
for index, example in enumerate(examples):
    plt.subplot(nb_examples+1, 6, 6*(index+1)+1)
    plt.imshow(image_list[example], cmap='gray', vmin=0, vmax=1)
    plt.axis("off")
    for k, kernel in enumerate(intermediate[index]):
        plt.subplot(nb_examples+1, 6, 6*(index+1)+2+k)
        plt.imshow(kernel, cmap='gray', vmin=0, vmax=2)
        plt.axis("off")
    plt.subplot(nb_examples+1, 6, 6*(index+1)+6)
    plt.imshow(outputs[index,0]>1e-2, cmap='gray', vmin=0, vmax=1)
    plt.axis("off")

plt.tight_layout()
plt.show()
