# CNN Intro Workshop

This repository contains a few example scripts written in Python to learn the basics of Convolutional Neural Networks, and is meant to be used as part of a workshop.

The only libraries required are **opencv**, **numpy**, **matplotlib**, and **pytorch**. (See `cnn_env.yml` to install a dedicated conda environment.)

The training and test data for the Muffin / Chihuahua classifier [can be found here](https://drive.google.com/file/d/1gD-pM9DEXIk2SAU8LsfIJ7OuADTMtIIk/view?usp=sharing "can be found here").
