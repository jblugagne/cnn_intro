import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt


#%% Ideal samples:

# Create "ideal" crosses
cross_1 = np.array([[0.0, 1.0, 0.0], [1.0, 1.0, 1.0], [0.0, 1.0, 0.0]])
cross_2 = np.array([[1.0, 0.0, 1.0], [0.0, 1.0, 0.0], [1.0, 0.0, 1.0]])

# Create "ideal" circles
circle_1 = np.array([[0.0, 1.0, 0.0], [1.0, 0.0, 1.0], [0.0, 1.0, 0.0]])
circle_2 = np.array([[1.0, 1.0, 1.0], [1.0, 0.0, 1.0], [1.0, 1.0, 1.0]])

plt.subplot(2, 2, 1)
plt.imshow(cross_1, cmap='binary')
plt.title('Cross 1')

plt.subplot(2, 2, 2)
plt.imshow(cross_2, cmap='binary')
plt.title('Cross 2')

plt.subplot(2, 2, 3)
plt.imshow(circle_1, cmap='binary')
plt.title('Circle 1')

plt.subplot(2, 2, 4)
plt.imshow(circle_2, cmap='binary')
plt.title('Circle 2')

plt.tight_layout()
plt.show()


#%% Noisy samples:

# Create stacks of samples:
nb_samples = 20_000
x = np.stack([cross_1, cross_2, circle_1, circle_2]*(nb_samples//4), axis=0)
y = np.stack([0.0, 0.0, 1.0, 1.0]*(nb_samples//4), axis=0)

# Add noise:
x += np.random.uniform(-1, 1, size=x.shape)
x = (x - x.min()) / x.ptp()

plt.subplot(2, 2, 1)
plt.imshow(x[0], cmap='binary')
plt.title('Cross 1')

plt.subplot(2, 2, 2)
plt.imshow(x[1], cmap='binary')
plt.title('Cross 2')

plt.subplot(2, 2, 3)
plt.imshow(x[2], cmap='binary')
plt.title('Circle 1')

plt.subplot(2, 2, 4)
plt.imshow(x[3], cmap='binary')
plt.title('Circle 2')

plt.tight_layout()
plt.show()

# Shuffle samples:
shuffling = np.random.permutation(nb_samples)
x = x[shuffling]
y = y[shuffling]

#%%

import torch
import torch.nn as nn
from torch.utils.data import DataLoader, TensorDataset

# Hyperparameters
input_size = 9
output_size = 1
batch_size = 1
learning_rate = 0.01
num_epochs = 1

# Create Tensor datasets
train_data = TensorDataset(
    torch.tensor(x.reshape(nb_samples, -1), dtype=torch.float32),
    torch.tensor(y, dtype=torch.float32).view(-1, 1)
)
train_loader = DataLoader(dataset=train_data, batch_size=batch_size, shuffle=True)

# Model definition
class SimpleNetwork(nn.Module):
    def __init__(self):
        super(SimpleNetwork, self).__init__()
        self.fc = nn.Linear(input_size, output_size, bias=False)

    def forward(self, x):
        # return self.fc(x)
        return torch.sigmoid(self.fc(x))

# Initialize the model
model = SimpleNetwork()

# Loss and optimizer
criterion = nn.BCELoss()
optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)

weights = model.fc.weight.detach().numpy()
weights = weights / weights.ptp()
plt.imshow(weights.reshape(3,3), cmap="RdBu", vmin=-1, vmax=1)
plt.title("Kernel")
plt.colorbar(extend='both')
plt.tight_layout()
plt.show()

#%% Training loop

for epoch in range(num_epochs):
    for i, (inputs, labels) in enumerate(train_loader):
        # Forward pass
        outputs = model(inputs)
        loss = criterion(outputs, labels)

        # Backward and optimize
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # Plot kernel weights every 200 steps:
        if (i+1) % 200 == 0:
            weights = model.fc.weight.detach().numpy()
            weights = weights / weights.ptp()
            plt.imshow(weights.reshape(3,3), cmap="RdBu", vmin=-1, vmax=1)
            plt.colorbar(extend='both')
            plt.title(f"Kernel, {i+1} learning steps")
            plt.tight_layout()
            plt.show()
            plt.pause(.1)

#%% Evaluate data:

model.eval()
with torch.no_grad():
    y_hat = model(torch.tensor(x.reshape(nb_samples, -1), dtype=torch.float32))
    y_hat = y_hat.round().detach().numpy()[:,0]

correct = (y_hat == y).sum()
print(f"Total prediction accuracy: {100*correct/nb_samples:.1f}%")

#%% Show random samples:

examples = np.random.choice(nb_samples, size=4, replace=False)
categories = {0: "Cross", 1: "Circle"}
for index, example in enumerate(examples):
    plt.subplot(2, 2, index+1)
    plt.imshow(x[example], cmap='binary')
    plt.title(f'Predicted: {categories[y_hat[example]]} - Truth: {categories[y[example]]}')
plt.tight_layout()
plt.show()
